//
//  main.m
//  WeatherApp
//
//  Created by Helmer, Paul on 22/07/2019.
//  Copyright © 2019 Helmer, Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
